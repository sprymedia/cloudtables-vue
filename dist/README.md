
# CloudTables for Vue 2

CloudTables Vue 2 is a Vue component compiled to work with your Vue 2 application. It adds a custom HTML tag `<CloudTables/>` to your Vue application.

[CloudTables](https://cloudtables.com/) is a no code / low code system which lets you create complex and dynamic database driven applications with ease. Hosted or self-hosted options are available so you can be up and running in moments.


## Getting Started

Inside the directory of your Vue application run:

```sh
npm install --save @cloudtables/vue2
```

After npm has finished installing. You can import you module in three different ways:

### Method 1:

Globally scoped in your main.js file

```js
import CloudTables from "@cloudtables/vue2";
Vue.use(CloudTables);
```

### Method 2:

Globally scoped in your main.js file

```js
import { component as CloudTables } from "@cloudtables/vue2";
Vue.component("CloudTables", CloudTables);
```

### Method 3:

Scoped in a component:

```js
import { component as CloudTables } from "@cloudtables/vue2";
export default {
  name: "App",
  components: {
    CloudTables,
  },
};
```


## Use

Once imported you can then use the custom CouldTables tag `<CloudTables/>`, for example:

```html
<CloudTables
  src="https://ct-examples.cloudtables.io/loader/4e9e8e3c-f448-11eb-8a3f-43eceac3195f/table/d"
  apiKey="AzG0e04UxhduaTAJjYC3Dgfr"
/>
```

- `data-src` The custom url for your CloudTable from the _Embed_ page for your data set.
- `data-apiKey` would be replaced by your API Key (see the _Security / API Keys_ section in your CloudTables application)
- `data-token` server side generated secure access token that can be used instead of an `apiKey`
- `data-userId` is optional, but will be used to uniquely identify user's in the CloudTables interface.
- `data-userName` is also optional, but can be used to help identify who made what changes when reviewing logs in CloudTables. It is recommended you include `userId` and `userName`.

All the data values required can be found in your [CloudTables](https://cloudtables.com/) application.
