
## Vue 2 and 3 components

This repo contains the two npm package source files for CloudTables' Vue components.

* `dist` contains the `@cloudtables/vue2` npm package as it need to be built to work within a Vue 2 application
* `src` contains the `@cloudtables/vue3` npm package with the uncomplied source files.

to build the vue 2 files run 'npm run build' in the main dir.
