import CloudTables from "./cloudtables.vue";

export default function install(Vue, options) {
  Vue.component("CloudTables", CloudTables);
}
export const component = CloudTables;
